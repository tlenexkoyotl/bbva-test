import { html, css } from "lit-element";
import { PageDM } from "../utils/page-dm.js";

import "@polymer/paper-card";

import "../bookCard";
import "../bookFetcher";

class HomePage extends PageDM {
  static get styles() {
    return css`
      .principal-container > * {
        max-width: 600px;
        margin-left: auto;
        margin-right: auto;
      }
    `;
  }

  receiveBook(e) {
    console.log(e.detail);

    const bookCard = this.shadowRoot.querySelector("book-card");

    bookCard.setAttribute("bookTitle", e.detail.title);
    bookCard.setAttribute("author", e.detail.uri);
  }

  render() {
    return html`
      <section class="principal-container">
        <h2>Welcome to Catsys</h2>
        <book-fetcher @book-received=${this.receiveBook}></book-fetcher>
        <book-card></book-card>
      </section>
    `;
  }
}

window.customElements.define("home-page", HomePage);
