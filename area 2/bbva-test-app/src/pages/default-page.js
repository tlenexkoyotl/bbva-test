import { html, css } from "lit-element";

import { PageDM } from "../utils/page-dm.js";

import "../alertComponent";
import "../counterDisplay";

class DefaultPage extends PageDM {
  static get styles() {
    return css`
      .principal-container > * {
        max-width: 600px;
        margin-left: auto;
        margin-right: auto;
      }
    `;
  }

  updateCounterDisplay(e) {
    const counterDisplay = this.shadowRoot.querySelector("counter-display");

    counterDisplay.setAttribute("counter", e.detail);
  }

  render() {
    return html`
      <section class="principal-container">
        <alert-component
          @counter-clicked=${this.updateCounterDisplay}
        ></alert-component>
        <counter-display></counter-display>
      </section>
    `;
  }
}

window.customElements.define("default-page", DefaultPage);
