import { LitElement, html, css } from "lit-element";

import "@polymer/paper-card";

export default class AlertComponent extends LitElement {
  static get properties() {
    return {
      counter: {
        type: Number,
      },
    };
  }

  static get styles() {
    return css`
      .principal-container > * {
        max-width: 600px;
        margin-left: auto;
        margin-right: auto;
      }

      paper-card {
        margin: 10px;
      }
    `;
  }

  constructor() {
    super();

    this.counter = 0;
  }

  countClick(e) {
    const event = new CustomEvent("counter-clicked", {
      detail: ++this.counter,
    });

    console.log(this.counter)

    this.dispatchEvent(event);
  }

  render() {
    return html`
      <section class="principal-container">
        <paper-card @click=${this.countClick}>
          <h1>Click me!</h1>
        </paper-card>
      </section>
    `;
  }
}

window.customElements.define("alert-component", AlertComponent);
