import { LitElement } from "lit-element";

import "@polymer/paper-card";

export default class BookFetcher extends LitElement {
  static get properties() {
    return {
      bookData: {
        type: Object,
        reflect: true,
      },
    };
  }

  constructor() {
    super();

    this.bookTitle = "";
  }

  connectedCallback() {
    fetch("https://api.datos.gob.mx/v2/Records")
      .then((response) => response.json())
      .then((json) => {
        this.bookData = {
          title: json.results[0].releases[0].publisher.name,
          uri: json.results[0].releases[0].publisher.uri,
        };

        const event = new CustomEvent("book-received", {
          detail: this.bookData,
        });

        this.dispatchEvent(event);
      });
  }
}

window.customElements.define("book-fetcher", BookFetcher);
