import { LitElement, html, css } from "lit-element";

import "@polymer/paper-card";

export default class CounterDisplay extends LitElement {
  static get properties() {
    return {
      counter: {
        type: Number,
        reflect: true,
      },
    };
  }

  static get styles() {
    return css`
      .principal-container > * {
        max-width: 600px;
        margin-left: auto;
        margin-right: auto;
      }

      paper-card {
        margin: 10px;
      }
    `;
  }

  constructor() {
    super();

    this.counter = 0;
  }

  countClick(e) {
    const event = new CustomEvent("counter-clicked", {
      detail: ++this.counter,
    });

    console.log(this.counter)

    this.dispatchEvent(event);
  }

  render() {
    return html`
      <section class="principal-container">
        <paper-card>
          <h1>${this.counter}</h1>
        </paper-card>
      </section>
    `;
  }
}

window.customElements.define("counter-display", CounterDisplay);
