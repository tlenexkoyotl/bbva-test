import { LitElement, html, css } from "lit-element";

import "@polymer/paper-card";

export default class BookCard extends LitElement {
  static get properties() {
    return {
      bookTitle: {
        type: String,
        reflect: true,
      },
      author: {
        type: String,
        reflect: true,
      },
    };
  }

  static get styles() {
    return css`
      .principal-container > * {
        max-width: 600px;
        margin-left: auto;
        margin-right: auto;
      }

      paper-card {
        margin: 10px;
      }
    `;
  }

  constructor() {
    super();

    this.bookTitle = "";
    this.author = "";
  }

  render() {
    return html`
      <section class="principal-container">
        <paper-card heading="${this.bookTitle}">
          <h1>${this.author}</h1>
        </paper-card>
      </section>
    `;
  }
}

window.customElements.define("book-card", BookCard);
