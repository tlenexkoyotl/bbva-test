/* ES5 */

/* return a new array containing only those people older than the specified age */
function filterOlderThan(people, age) {
  return people
    .filter(function (person) {
      return person.age >= age;
    })
    .map(function (person) {
      return Object.assign({}, person);
    });
}

/* mark filtered people as enabled */
function markPeopleAsEnabled(people) {
  return people.map(function (person) {
    return Object.assign({ enabled: true }, person);
  });
}

const people = [
  {
    age: 20,
  },
  {
    age: 29,
  },
  {
    age: 37,
  },
  {
    age: 43,
  },
  {
    age: 57,
  },
];

const filteredPeople = filterOlderThan(people, 30);

console.log({ filteredPeople });

const enabledPeople = markPeopleAsEnabled(filteredPeople);

console.log({ enabledPeople });
