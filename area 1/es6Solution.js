/* ES6 */

/* return a new array containing only those people older than the specified age */
const filterOlderThan = (people, age) =>
  people
    .filter((person) => person.age >= age)
    .map((person) => Object.assign({}, person));

/* mark filtered people as enabled */
const markPeopleAsEnabled = (people) =>
  people.map((person) => Object.assign({ enabled: true }, person));

const people = [
  {
    age: 20,
  },
  {
    age: 29,
  },
  {
    age: 37,
  },
  {
    age: 43,
  },
  {
    age: 57,
  },
];

const filteredPeople = filterOlderThan(people, 30);

console.log({ filteredPeople });

const enabledPeople = markPeopleAsEnabled(filteredPeople);

console.log({ enabledPeople });
