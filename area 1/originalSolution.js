/* Original */

function xxxx(coleccion, edad) {
  return coleccion.filter((item) => {
    return item.edad >= edad;
  });
}

function marcaHabilitados(coleccion) {
  return coleccion.map((item) => {
    item.habilitado = true;
    return item;
  });
}

const personas = [
  {
    edad: 20,
  },
  {
    edad: 29,
  },
  {
    edad: 37,
  },
  {
    edad: 43,
  },
  {
    edad: 57,
  },
];

const filtrados = xxxx(personas, 30);

console.log({ filtrados });

const habilitados = marcaHabilitados(personas);

console.log({ habilitados });
